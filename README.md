<h1 align="left">
  <img src="https://scontent.fcgh8-1.fna.fbcdn.net/v/t39.30808-6/289976373_1831753467021092_1415066520732617_n.jpg?_nc_cat=103&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeHemTm85mGYGpEQE56ucVfxXf8q3M7fx65d_yrczt_HrtiTJxnjL4TKNWLpXSrAbicqtQZR1gObqpPUfH2jro06&_nc_ohc=1JQFZXHIX5sAX-PzHkv&_nc_ht=scontent.fcgh8-1.fna&oh=00_AT8mmJn7emQopNDKuBMeB-y9j7MPhwbSFLdGRpG5ZKrZ2A&oe=62B9E6F3" title="Kimchi Logo" width="400" />
</h1>

<p>Projeto elaborado para o desafio final do iLab, programa de aceleração de talentos em tech.
Consiste em um sistema que permite ao administrador, devidamente autenticado e autorizado, cadastrar e consultar pedidos e clientes. Foi realizado com uso de microsserviços, mensageria e deploy por pipeline em Kubernetes.</p>

- Para ver o repositório da **API Auth**, clique [aqui](https://code.ifoodcorp.com.br/amaral.carolina/g3-auth)

- Para ver o repositório da **API User**, clique [aqui](https://code.ifoodcorp.com.br/amaral.carolina/g3-user)

- Para ver o repositório da **API Order**, clique [aqui](https://code.ifoodcorp.com.br/amaral.carolina/g3-order)

- Para ver o repositório do **Consumer Kafka**, clique [aqui](https://code.ifoodcorp.com.br/debora.brum/g3-consumer)

- Para ver o repositório de **Front-end**, clique [aqui](https://code.ifoodcorp.com.br/amaral.carolina/g3-front)

- Para acessar a aplicação diretamente no seu browser, acesse: http://g3kimchi.tk

### 👩🏽‍💻 Pessoas Desenvolvedoras

- [Ana Carolina Amaral](https://github.com/anacapx)
- [Alessandro Costa](https://github.com/ab-costa)
- [Debora Brum](https://github.com/DeboraBrum)
- [Ester Lourenco](https://github.com/elolourenco)
- [Lisandre Andreolo](https://github.com/lisdrl)

## 💡 Mentor

- [Rafael Oliveira](https://www.linkedin.com/in/rafaelsomartins/)

